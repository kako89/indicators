# Indicaciones para ejecutar componente back para proyecto de Indicator

## Requisitos

Este proyecto fue generado Apache Maven 3.6.3, Spring Boot 2.3.1 y java 8.


## Compilación y Ejecución del proyecto

Para ejecutar el proyecto, se debe abrir una terminal y situarse dentro de la carpeta del proyecto.
Una vez dentro de la carpeta, ejecutar el comando "mvn clean install", proceso que hará la compilación del proyecto y generará un archivo WAR
dentro de la carpeta "/target" que se genera en el proyecto.

Este componente WAR es el que debe ser desplegado en un servidor de aplicaciones como Apache Tomcat o Weblogic de manera manual.

Por otro lado, este proyecto trae un servidor de aplicaciones Tomcat embebido, por lo que se puede levantar un servidor local ejecutando el
comando "mvn spring-boot:run", corriendo por defecto el servidor en el puerto 8080.

## Notas Importantes


Para la muestra de datos desde el proyecto front, se debe desplegar el componente WAR del proyecto en un servidor local escuchando las peticiones por el puerto 8080.
