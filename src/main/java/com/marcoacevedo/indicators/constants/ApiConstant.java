package com.marcoacevedo.indicators.constants;

public class ApiConstant {
	
	public static final String URL_API_LAST = "https://www.indecon.online/last";
	public static final String URL_API_ALL_VALUES = "https://www.indecon.online/values";
	
}
