package com.marcoacevedo.indicators.controller;


import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.marcoacevedo.indicators.domain.Indicator;
import com.marcoacevedo.indicators.domain.IndicatorValuesResponse;
import com.marcoacevedo.indicators.service.IndicatorService;


@RestController
@RequestMapping("/indicator")
@CrossOrigin(origins = "*")
public class IndicatorController {
	
	private static final Log LOG = LogFactory.getLog(IndicatorController.class);
	
	@Autowired
	private IndicatorService indicatorService;
	
	@GetMapping("/last")
	public List<Indicator> lastIndicatorsValue(){
		
		
		return indicatorService.lastValues();
	}
	
	
	@GetMapping("/values")
	public IndicatorValuesResponse getAllValues(
			@RequestParam(name="key", required=true) String key){
		
		IndicatorValuesResponse indicatorValuesResponse= indicatorService.getAllValues(key);
		
		return indicatorValuesResponse;
	}

}
