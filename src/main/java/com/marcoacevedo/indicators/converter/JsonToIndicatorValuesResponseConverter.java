package com.marcoacevedo.indicators.converter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.marcoacevedo.indicators.domain.IndicatorValueDate;
import com.marcoacevedo.indicators.domain.IndicatorValuesResponse;

@Component
public class JsonToIndicatorValuesResponseConverter {
	
	public IndicatorValuesResponse convert(JSONObject json) {
		
		IndicatorValuesResponse indicatorValuesResponse = new IndicatorValuesResponse();
		indicatorValuesResponse.setKey(json.getString("key"));
		indicatorValuesResponse.setName(json.getString("name"));
		indicatorValuesResponse.setUnit(json.getString("unit"));
		
		JSONObject values=(JSONObject) json.get("values");
		Iterator<String> keys = values.keys();
		
		List<IndicatorValueDate> indicatorValueList = new ArrayList<IndicatorValueDate>();
		
		while(keys.hasNext()) {
			String keyJson = keys.next();
			
			IndicatorValueDate indicatorValueDate = new IndicatorValueDate();
			
			indicatorValueDate.setDate(Long.parseLong(keyJson));
			indicatorValueDate.setValue(values.getFloat(keyJson));
			indicatorValueList.add(indicatorValueDate);
		}
		
		indicatorValuesResponse.setValues(indicatorValueList);
		
		return indicatorValuesResponse;
	}

}
