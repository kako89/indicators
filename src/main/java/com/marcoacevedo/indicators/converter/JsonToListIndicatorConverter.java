package com.marcoacevedo.indicators.converter;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONObject;
import org.springframework.stereotype.Component;

import com.marcoacevedo.indicators.domain.Indicator;

@Component
public class JsonToListIndicatorConverter {

	
	public List<Indicator> convert(JSONObject json){
		
		List<Indicator> indicators = new ArrayList<Indicator>();
		
		Iterator<String> keys = json.keys();
		
		
		while(keys.hasNext()) {
			String key = keys.next();
			if(json.get(key) instanceof JSONObject) {
				
				JSONObject jsonIndicator = (JSONObject) json.get(key);
				
				Indicator indicator = new Indicator();
				indicator.setKey(jsonIndicator.getString("key"));
				indicator.setName(jsonIndicator.getString("name"));
				indicator.setUnit(jsonIndicator.getString("unit"));
				indicator.setValue(jsonIndicator.getFloat("value"));
				indicator.setDate(jsonIndicator.getLong("date"));
				
				indicators.add(indicator);
				
			}
		}
		
		return indicators;
		
	}
}
