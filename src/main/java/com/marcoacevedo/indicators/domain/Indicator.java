package com.marcoacevedo.indicators.domain;

public class Indicator {
	
	private String key;
	private String name;
	private String unit;
	private long date;
	private float value;
	
	public Indicator() {}
	
	

	public Indicator(String key, String name, String unit, long date, float value) {
		super();
		this.key = key;
		this.name = name;
		this.unit = unit;
		this.date = date;
		this.value = value;
	}



	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public long getDate() {
		return date;
	}

	public void setDate(long date) {
		this.date = date;
	}

	public float getValue() {
		return value;
	}

	public void setValue(float value) {
		this.value = value;
	}



	@Override
	public String toString() {
		return "Indicator [key=" + key + ", name=" + name + ", unit=" + unit + ", date=" + date + ", value=" + value
				+ "]";
	}
	
	
	
	
	
	
	
	
	

}
