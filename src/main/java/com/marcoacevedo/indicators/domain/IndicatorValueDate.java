package com.marcoacevedo.indicators.domain;

public class IndicatorValueDate {
	private long date;
	private float value;
	
	
	
	public IndicatorValueDate() {
		
	}
	
	
	public IndicatorValueDate(long date, float value) {
		super();
		this.date = date;
		this.value = value;
	}
	public long getDate() {
		return date;
	}
	public void setDate(long date) {
		this.date = date;
	}
	public float getValue() {
		return value;
	}
	public void setValue(float value) {
		this.value = value;
	}
	
}
