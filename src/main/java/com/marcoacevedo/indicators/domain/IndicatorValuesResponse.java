package com.marcoacevedo.indicators.domain;

import java.util.List;

public class IndicatorValuesResponse {
	
	private String key;
	private String name;
	private String unit;
	private List<IndicatorValueDate> values;
	
	public IndicatorValuesResponse() {
		
	}

	public String getKey() {
		return key;
	}

	public void setKey(String key) {
		this.key = key;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public List<IndicatorValueDate> getValues() {
		return values;
	}

	public void setValues(List<IndicatorValueDate> values) {
		this.values = values;
	}

	
	

}
