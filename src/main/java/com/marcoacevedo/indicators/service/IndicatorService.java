package com.marcoacevedo.indicators.service;


import java.util.List;

import com.marcoacevedo.indicators.domain.Indicator;
import com.marcoacevedo.indicators.domain.IndicatorValuesResponse;


public interface IndicatorService {
	
	public List<Indicator> lastValues();
	public IndicatorValuesResponse getAllValues(String key);
	
}
