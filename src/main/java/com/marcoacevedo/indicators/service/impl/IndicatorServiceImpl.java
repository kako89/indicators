package com.marcoacevedo.indicators.service.impl;


import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;


import com.marcoacevedo.indicators.constants.ApiConstant;
import com.marcoacevedo.indicators.converter.JsonToIndicatorValuesResponseConverter;
import com.marcoacevedo.indicators.converter.JsonToListIndicatorConverter;
import com.marcoacevedo.indicators.domain.Indicator;
import com.marcoacevedo.indicators.domain.IndicatorValuesResponse;
import com.marcoacevedo.indicators.service.IndicatorService;
import com.marcoacevedo.indicators.util.Utils;

@Service
public class IndicatorServiceImpl implements IndicatorService {

	private static final Log LOG = LogFactory.getLog(IndicatorServiceImpl.class);

	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	private JsonToIndicatorValuesResponseConverter jtivrc;
	
	@Autowired
	private JsonToListIndicatorConverter jtlic;
	
	@Autowired
	private Utils utils;

	@Override
	public List<Indicator>  lastValues() {
		
		LOG.info("---------------Inicio IndicatorService-lastValues----------------");
		
		
		String url = ApiConstant.URL_API_LAST;
		
		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, utils.getHeaders(), String.class);
		
		JSONObject json = new JSONObject(response.getBody());
		
		LOG.info("---------------Termino IndicatorService-lastValues----------------");
		
		return jtlic.convert(json);
	}


	@Override
	public IndicatorValuesResponse getAllValues(String key) {
		
		LOG.info("---------------Inicio IndicatorService-getAllValues----------------");

		String url = ApiConstant.URL_API_ALL_VALUES + "/" + key;


		ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, utils.getHeaders(), String.class);
		
        JSONObject json = new JSONObject(response.getBody());
        
		
		LOG.info("---------------Termino IndicatorService-getAllValues----------------");

		return jtivrc.convert(json);
	}


}
