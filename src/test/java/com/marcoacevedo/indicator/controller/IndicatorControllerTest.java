package com.marcoacevedo.indicator.controller;

import static org.assertj.core.api.Assertions.assertThat;


import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.marcoacevedo.indicators.IndicatorsApplication;
import com.marcoacevedo.indicators.controller.IndicatorController;
import com.marcoacevedo.indicators.domain.Indicator;
import com.marcoacevedo.indicators.service.IndicatorService;



@RunWith(SpringRunner.class)
@SpringBootTest(classes = IndicatorsApplication.class)
public class IndicatorControllerTest {
	
	
	@Autowired
	private IndicatorController indicatorController;
	
	@Autowired
	private IndicatorService indicatorService;
	
	@Test
	public void lastValuesTest() {
		
		List<Indicator> indicators = new ArrayList<Indicator>();
		indicators = indicatorController.lastIndicatorsValue();
		assertThat(indicators.size()).isGreaterThan(0);
	}
	
	

}
