package com.marcoacevedo.indicators.service;


import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.*;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.marcoacevedo.indicators.converter.JsonToIndicatorValuesResponseConverter;
import com.marcoacevedo.indicators.converter.JsonToListIndicatorConverter;
import com.marcoacevedo.indicators.domain.Indicator;
import com.marcoacevedo.indicators.domain.IndicatorValuesResponse;
import com.marcoacevedo.indicators.service.impl.IndicatorServiceImpl;
import com.marcoacevedo.indicators.util.Utils;
import com.marcoacevedo.indicators.service.IndicatorService;


@RunWith(SpringRunner.class)
@SpringBootTest
public class IndicatorServiceTest {
	
	@Autowired
	private IndicatorService indicatorService;

	
	@Test
	public void lastValuesTest() {
		List<Indicator> lastValues = indicatorService.lastValues();
		assertThat(lastValues.size()).isGreaterThan(0);
	}
	
	@Test
	public void getAllValuesTest() {
		IndicatorValuesResponse indicatorValueResponse = indicatorService.getAllValues("cobre");
		assertThat(indicatorValueResponse.getKey()).isNotEmpty();
		assertThat(indicatorValueResponse.getName()).isNotEmpty();
		assertThat(indicatorValueResponse.getUnit()).isNotEmpty();
		assertThat(indicatorValueResponse.getValues().size()).isGreaterThan(0);
		
	}

}
